#include <stdio.h>
#include <mem.h>

typedef struct {
    char MaSinhVien[10];
    char Ten[10];
    char SoDienThoai[10];
} SinhVien;

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

void Menu() {
    printf("1. Them moi sinh vien \n");
    printf("2. Hien thi danh sach sinh vien \n");
    printf("3. Luu danh sach sinh vien \n");
    printf("4. Doc danh sach sinh vien tu file \n");
    printf("5. Thoat chuong trinh \n");
    printf("Nhap lua chon cua ban:  \n");
}

void ThemMoiSinhVien(SinhVien SinhViens[], int i) {
    do {
        printf("Nhap ma sinh vien:\n");
        fgets(SinhViens[i].MaSinhVien, sizeof(SinhViens[i].MaSinhVien) * sizeof(char), stdin);
        removeStdChar(SinhViens[i].MaSinhVien);
        SinhViens[i].MaSinhVien[strlen(SinhViens[i].MaSinhVien) - 1] = ' ';
        if (strlen(SinhViens[i].MaSinhVien) != 6) {
            printf("Loi, Nhap lai ma sinh vien\n");
        }
    } while (strlen(SinhViens[i].MaSinhVien) != 6);
    printf("Nhap Ten sinh vien:\n");
    fgets(SinhViens[i].Ten, sizeof(SinhViens[i].Ten) * sizeof(char), stdin);
    removeStdChar(SinhViens[i].Ten);
    SinhViens[i].Ten[strlen(SinhViens[i].Ten) - 1] = ' ';
    printf("Nhap so dien thoai sinh vien:\n");
    fgets(SinhViens[i].SoDienThoai, sizeof(SinhViens[i].SoDienThoai) * sizeof(char), stdin);
    removeStdChar(SinhViens[i].SoDienThoai);
    SinhViens[i].SoDienThoai[strlen(SinhViens[i].SoDienThoai) - 1] = ' ';

}

void HienThiDanhSachSinhVien(SinhVien SinhViens[], int i) {
    printf("\n%-15s| %-15s| %15s\n", "Ma Sinh Vien", "Ten", "So Dien Thoai");
    for (int j = 0; j < 2; j++) {
        printf("\n%-15s| %-15s| %15s\n", SinhViens[j].MaSinhVien, SinhViens[j].Ten, SinhViens[j].SoDienThoai);
    }
}

int main() {
    int luachon;
    int soLuong = 0;
    SinhVien SinhViens[2];
    char DanhSach[1000];
    FILE *fp;
    while (1 == 1) {
        Menu();
        scanf("%d", &luachon);
        getchar();
        switch (luachon) {
            case 1:
                if (soLuong < 2) {
                    ThemMoiSinhVien(SinhViens, soLuong);
                    soLuong++;
                } else {
                    printf("Danh sach sinh vien da day \n");
                    break;
                }
                break;
            case 2:
                HienThiDanhSachSinhVien(SinhViens, soLuong);
                break;
            case 3:
                fp = fopen("DanhSachSinhVien.txt", "w");
                fprintf(fp, "\n%-15s|%-15s|%-15s\n", "Ma Sinh Vien", "Ten", "So Dien Thoai");
                for (int j = 0; j < 2; j++) {
                    fprintf(fp, "\n%-15s|%-15s|%-15s\n", SinhViens[j].MaSinhVien, SinhViens[j].Ten, SinhViens[j].SoDienThoai);
                }
                fclose(fp);
                break;
            case 4:
                fp = fopen("DanhSachSinhVien.txt", "r");
                while (fgets(DanhSach, 1000, fp) != NULL) {
                    printf("%s", DanhSach);
                }
                fclose(fp);
                break;
            case 5:
                return 0;
            default:
                printf("Lua chon sai. Xin moi nhap lai: \n");
                break;
        }
    }
}